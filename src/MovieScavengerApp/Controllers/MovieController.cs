﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Infusion.MovieScavengerApp.Services;
using Infusion.MovieScavengerApp.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Infusion.MovieScavengerApp.Controllers
{
    public class MovieController : Controller
    {
        // GET: /<controller>/

        private IMoviesService _moviesService;
        
        public MovieController(IMoviesService moviesService) { _moviesService = moviesService; } // *** działa bo w serviceconfig dodaliśmy tą linijkę
                
        public IActionResult Index()
        {
            return View();
        }

        // DONE: Snippet 4
        [HttpPost]
        public IActionResult AddMovie(string title, string releaseDate)
        {
            // Extra task: implement input validation
            // DONE: insert snippet 5
            _moviesService.AddMovie(new Movie
            {
                Title = title,
                ReleaseDate = DateTime.Parse(releaseDate)
            });
            return Content("Movie Added"); // Extra task: redirect to Home/Index instead
        }
    }
}
