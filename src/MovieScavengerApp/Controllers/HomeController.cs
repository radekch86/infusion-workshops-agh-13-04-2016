﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infusion.MovieScavengerApp.Services;
using Infusion.MovieScavengerApp.ViewModels;
using Microsoft.AspNet.Mvc;

namespace Infusion.MovieScavengerApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMoviesService _moviesService;

        public HomeController(IMoviesService moviesService)
        {
            _moviesService = moviesService;
        }

        public IActionResult Index()
        {
            var movieViewModels = new List<MovieViewModel>();
            foreach (var movie in _moviesService.GetMovies())
            {
                movieViewModels.Add(new MovieViewModel
                {
                    Title = movie.Title,
                    ReleaseDate = movie.ReleaseDate.ToString("d"),
                    // Extra task: separate each actor with new line and add BirthDate information for each actor
                    Actors = movie.Cast != null ? string.Join(", ", movie.Cast.Select(t => t.Name)) : null
                });
            }

            return View(movieViewModels);
        }
    }
}