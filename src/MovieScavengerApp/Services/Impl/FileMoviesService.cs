﻿using System.Collections.Generic;
using Infusion.MovieScavengerApp.Models;

namespace Infusion.MovieScavengerApp.Services.Impl
{
    // Extra task: implement file-backed movies service for file wwwroot/Movies.txt to persist changes
    public class FileMoviesService : IMoviesService
    {
        public IList<Movie> GetMovies()
        {
            throw new System.NotImplementedException();
        }

        public void AddMovie(Movie movie)
        {
            throw new System.NotImplementedException();
        }
    }
}