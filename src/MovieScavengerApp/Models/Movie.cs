﻿using System;
using System.Collections.Generic;

namespace Infusion.MovieScavengerApp.Models
{
    public class Movie
    {
        public string Title { get; set; }
        
        public DateTime ReleaseDate { get; set; }
        
        public IList<Actor> Cast { get; set; }
    }
}