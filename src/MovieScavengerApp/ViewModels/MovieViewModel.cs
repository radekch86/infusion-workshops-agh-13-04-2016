﻿using System;
using System.Collections.Generic;
using Infusion.MovieScavengerApp.Models;

namespace Infusion.MovieScavengerApp.ViewModels
{
    public class MovieViewModel
    {
        public string Title { get; set; }

        public string ReleaseDate { get; set; }

        public string Actors { get; set; }
    }
}